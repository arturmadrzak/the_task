#include <stdio.h>
#include <stdlib.h>

unsigned int hysteresis(unsigned int input_percent) {
    static unsigned int prev = 0;
    unsigned int result;
    int diff = abs(prev-input_percent);
    if (diff > 25)
        result = (input_percent + 12)/25;
    else {
        if (input_percent > prev)
            result = (input_percent + 10)/25;
        else 
            result = (input_percent + 15 - 1)/25;
    }
    prev = input_percent;
    return result;
}

int main() {
    unsigned int i;
    unsigned int enc;
    while (1) {
        printf("Enter the value [0,100]: ");
        scanf("%u", &i);
        if (i > 100) {
            fprintf(stderr, "Error! Provide value in [0,100] range\n");
            exit(EXIT_FAILURE);
        }
        enc = hysteresis(i);
        printf("\nEncoded value is: %u\n", enc);
    }
    return 0;
}
