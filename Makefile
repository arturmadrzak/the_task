SRCS = $(wildcard *.c)
OBJS = $(SRCS:.c=.o)
DEPS = $(SRCS:.c=.d)

CFLAGS = -Wall -fPIC -MMD

.PHONY: all lib test clean

all: main test

main: $(OBJS)

lib: libapp.a

test: lib
	make -C tests

libapp.a: $(OBJS)
	$(AR) csr $@ $^

clean:
	make -C tests clean
	rm -rf $(TESTDIR)
	rm -f $(OBJS)
	rm -f $(DEPS)
	rm -f libapp.a
	rm main

include $(wildcard *.d)
