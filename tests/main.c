#include <cgreen/cgreen.h>
#include <cgreen/mocks.h>

extern unsigned int hysteresis(unsigned int input_percent);

Describe(main);

BeforeEach(main) 
{
}

AfterEach(main) 
{
}

Ensure(main, encode_increase)
{
    assert_that(hysteresis(0), is_equal_to(0));
    assert_that(hysteresis(15), is_equal_to(1));
    assert_that(hysteresis(40), is_equal_to(2));
    assert_that(hysteresis(65), is_equal_to(3));
    assert_that(hysteresis(90), is_equal_to(4));
}

Ensure(main, encode_decrease)
{
    assert_that(hysteresis(100), is_equal_to(4));
    assert_that(hysteresis(85), is_equal_to(3));
    assert_that(hysteresis(60), is_equal_to(2));
    assert_that(hysteresis(35), is_equal_to(1));
    assert_that(hysteresis(10), is_equal_to(0));
}


Ensure(main, given_test_5_16_11_10)
{
    assert_that(hysteresis(5), is_equal_to(0));
    assert_that(hysteresis(16), is_equal_to(1));
    assert_that(hysteresis(11), is_equal_to(1));
    assert_that(hysteresis(10), is_equal_to(0));
}

Ensure(main, more_than_one_threshold_increase)
{
    assert_that(hysteresis(5), is_equal_to(0));
    assert_that(hysteresis(38), is_equal_to(2));

    assert_that(hysteresis(5), is_equal_to(0));
    assert_that(hysteresis(37), is_equal_to(1));
}

Ensure(main, more_than_one_threshold_decrease)
{
    assert_that(hysteresis(99), is_equal_to(4));
    assert_that(hysteresis(62), is_equal_to(2));

    assert_that(hysteresis(99), is_equal_to(4));
    assert_that(hysteresis(63), is_equal_to(3));
}

Ensure(main, hist_one_step_full_increment)
{
    assert_that(hysteresis(85), is_equal_to(3));
    assert_that(hysteresis(86), is_equal_to(3));
    assert_that(hysteresis(87), is_equal_to(3));
    assert_that(hysteresis(88), is_equal_to(3));
    assert_that(hysteresis(89), is_equal_to(3));
    assert_that(hysteresis(90), is_equal_to(4));
}

Ensure(main, hist_one_step_full_decrement)
{
    assert_that(hysteresis(90), is_equal_to(4));
    assert_that(hysteresis(89), is_equal_to(4));
    assert_that(hysteresis(88), is_equal_to(4));
    assert_that(hysteresis(87), is_equal_to(4));
    assert_that(hysteresis(86), is_equal_to(4));
    assert_that(hysteresis(85), is_equal_to(3));
}


