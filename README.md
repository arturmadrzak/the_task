[![pipeline status](https://gitlab.com/arturmadrzak/the_task/badges/master/pipeline.svg)](https://gitlab.com/arturmadrzak/the_task/commits/master)

# License
The **c-template** is available under the MIT License.

Powered by https://cgreen-devs.github.io
